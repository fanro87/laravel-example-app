<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    // unica ruta
    public function __invoke(){
        //return view('welcome');
        //return "Bienvenidos a la pagina principal";
        return view('home');
    }
}
