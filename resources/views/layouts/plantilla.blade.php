<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('title')</title>
        {{-- <script src="https://cdn.tailwindcss.com"></script> --}}
        <!-- favicon -->
        <!-- estilos -->
    </head>
    <body >
        <!-- header -->
        <!-- nav -->
        @yield('content')
        <!-- footer -->
        <!-- scripts -->
    </body>
</html>
